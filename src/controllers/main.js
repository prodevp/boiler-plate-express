import express from 'express';

const router = express.Router();

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('main', { title: 'Boiler-Plate' });
});

export default router;
