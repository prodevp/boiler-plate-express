import mongoose from 'mongoose';

const dbConnect = () => {
  if (mongoose.connection.readyState !== 2) {
    mongoose.connect('mongodb://testuser:testpass12@ds145573.mlab.com:45573/boilerplate', {
      useNewUrlParser: true,
    });
    const db = mongoose.connection;
    db.once('open', () => {
      console.log(`Connected to mongoose with state : ${mongoose.connection.readyState}`);
    });
  }
};
export default dbConnect;
