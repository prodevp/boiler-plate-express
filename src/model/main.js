import mongoose from 'mongoose';

const mainSchema = mongoose.Schema({
  name: String,
});

export default mongoose.model('MainSchema', mainSchema);
