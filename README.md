# Express

[![N|Solid](http://mean.io/wp-content/themes/twentysixteen-child/images/nodejs.png)](https://expressjs.com/)

Basic nodejs(express js) boilerplate with es6 support.

  - Support ES6 syntax
  - ESLint support
  - Babel installed
  - ejs View
  - nodemon

#  Features!

  - Import file using import instead of required.
  - Linting configured

### Installation

Boilerplate requires [Node.js](https://nodejs.org/) v6+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ cd application
$ npm install
$ npm start
```

For restart after changes...

```sh
$ rs
```

### Modules

Modlules used in Boilerplate.

| Plugin | README |
| ------ | ------ |
| nodemon | [https://github.com/remy/nodemon] |
| babel-preset-env | [https://babeljs.io/docs/en/env]|
| eslint | [https://eslint.org/] |
| eslint-config-airbnb-base | [https://www.npmjs.com/package/eslint-config-airbnb-base]|

### Development

Want to contribute? Great!
create pull request to git.

Verify the deployment by navigating to your server address in your preferred browser.

```sh
127.0.0.1:3000
```

License
----

MIT


**Free Software, Hell Yeah!**

