import createError from 'http-errors';
import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import http from 'http';
import indexRouter from './src/controllers/main';
import dbConnect from './src/config/db-connection';

const app = express();
dbConnect();
// view engine setup
app.set('views', path.join(`${__dirname}/src`, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
const port = process.env.PORT || '3000';
const server = http.createServer(app);
server.listen(port, 'localhost', (error) => {
  if (error) {
    console.error('Unable to listen on port', port, error);
  }
  console.log('Server listen on port', port);
});
export default app;
